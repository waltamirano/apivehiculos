package com.VehiculoApi;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


//@RestController
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(VehiculoApiApplication.class);
    }

//    public static void main(String[] args) {
//        SpringApplication.run(ServletInitializer.class, args);
//    }
//
//    @RequestMapping(value = "/")
//    public String hello() {
//        return "Hello World from Tomcat";
//    }
}
