/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.repository;

import com.VehiculoApi.model.Orden;
import com.VehiculoApi.model.Vehiculos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wiston.altamirano
 */
@Repository
public interface OrdenRepository extends CrudRepository<Orden, Long>{
    
}
