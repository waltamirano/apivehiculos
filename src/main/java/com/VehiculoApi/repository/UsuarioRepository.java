/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.repository;

import com.VehiculoApi.model.seguridad.Usuarios;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author wiston.altamirano
 */
@Repository
public interface UsuarioRepository extends CrudRepository<Usuarios, Long> {

    @Query(nativeQuery = false,value = "select us from Usuarios us where us.userName=?1")
    Usuarios getByUserName(String userName);
}
