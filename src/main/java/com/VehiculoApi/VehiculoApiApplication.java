package com.VehiculoApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehiculoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VehiculoApiApplication.class, args);
	}

}
