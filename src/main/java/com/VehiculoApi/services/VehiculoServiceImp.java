/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Persona;
import com.VehiculoApi.model.Vehiculos;
import com.VehiculoApi.repository.ClientesRepository;
import com.VehiculoApi.repository.PersonaRepository;
import com.VehiculoApi.repository.VehiculoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wiston.altamirano
 */
@Service
public class VehiculoServiceImp implements IVehiculoService {

    @Autowired
    private VehiculoRepository vehiculoRepository;

    @Override
    public List<Vehiculos> lstVehiculo() {
        return (List<Vehiculos>) vehiculoRepository.findAll();
    }

    @Override
    public Vehiculos agregar(Vehiculos vehiculo) {
        return vehiculoRepository.save(vehiculo);
    }

    @Override
    public Vehiculos editar(Vehiculos vehiculo) {
        return vehiculoRepository.save(vehiculo);
    }

    @Override
    public Vehiculos remover(Vehiculos vehiculo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Vehiculos buscarID(Long idVehiculo) {
        
        Vehiculos vh =vehiculoRepository.findById(idVehiculo).get();
        return vh;
    }

}
