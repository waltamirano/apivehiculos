/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.seguridad.Usuarios;
import java.util.List;

/**
 *
 * @author wiston.altamirano
 */
public interface IUsuarioServices {

    List<Usuarios> lstUsuarios();

    Usuarios buscarUserName(String userName);

    Usuarios agregar(Usuarios usuario);

}
