/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Persona;
import java.util.List;

/**
 *
 * @author wiston.altamirano
 */
public interface IPersonaService {

    List<Persona> lstPersona();

    Persona agregar(Persona persona);

    Persona editar(Persona persona);

    Persona remover(Persona persona);
}
