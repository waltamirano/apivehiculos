/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.seguridad.Usuarios;
import com.VehiculoApi.repository.UsuarioRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wiston.altamirano
 */
@Service
public class UsuarioServiceImp implements IUsuarioServices {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuarios buscarUserName(String userName) {
        return usuarioRepository.getByUserName(userName);
    }

    @Override
    public Usuarios agregar(Usuarios usuario) {
        return usuarioRepository.save(usuario);
    }

    @Override
    public List<Usuarios> lstUsuarios() {
        return (List<Usuarios>) usuarioRepository.findAll();
    }

}
