/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Persona;
import com.VehiculoApi.repository.ClientesRepository;
import com.VehiculoApi.repository.PersonaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wiston.altamirano
 */
@Service
public class PersonaServiceImp implements IPersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Override
    public List<Persona> lstPersona() {
      return (List<Persona>) personaRepository.findAll();
    }

    @Override
    public Persona agregar(Persona persona) {
        return personaRepository.save(persona);
    }

    @Override
    public Persona editar(Persona persona) {
        return personaRepository.save(persona);
    }

    @Override
    public Persona remover(Persona persona) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
