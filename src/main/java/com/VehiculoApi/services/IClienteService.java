/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Cliente;
import com.VehiculoApi.model.Persona;
import java.util.List;

/**
 *
 * @author wiston.altamirano
 */
public interface IClienteService {

    List<Cliente> lstClientes();

    Object buscarPorId(Long Id);
    
    Cliente agregar(Cliente cliente);

    Cliente editar(Cliente cliente);

    Cliente remover(Cliente cliente);
}
