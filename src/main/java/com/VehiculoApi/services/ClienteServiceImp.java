/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Cliente;
import com.VehiculoApi.repository.ClientesRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wiston.altamirano
 */
@Service
public class ClienteServiceImp implements IClienteService {

    @Autowired
    private ClientesRepository clientesRepository;

    @Override
    public List<Cliente> lstClientes() {
        return (List<Cliente>) clientesRepository.findAll();
    }

    @Override
    public Cliente agregar(Cliente cliente) {
        return clientesRepository.save(cliente);
    }

    @Override
    public Cliente editar(Cliente cliente) {
        return clientesRepository.save(cliente);
    }

    @Override
    public Cliente remover(Cliente cliente) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buscarPorId(Long Id) {
        
        return clientesRepository.findById(Id);
    }

}
