/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Orden;
import com.VehiculoApi.repository.OrdenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wiston.altamirano
 */
@Service
public class OrdenServiceImp implements IOrdenesService {

    @Autowired
    OrdenRepository ordenRepository;
    
    @Override
    public Object bucarOderenID(Long id) {
        return ordenRepository.findById(id);
    }

    @Override
    public Orden guardar(Orden orden) {
       return  ordenRepository.save(orden);
    }

}
