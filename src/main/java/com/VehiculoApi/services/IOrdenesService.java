/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Orden;

/**
 *
 * @author wiston.altamirano
 */
public interface IOrdenesService {
    
    Object bucarOderenID(Long id);
    
    Orden guardar(Orden orden);
}
