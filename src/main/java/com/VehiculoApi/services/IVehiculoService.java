/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.services;

import com.VehiculoApi.model.Persona;
import com.VehiculoApi.model.Vehiculos;
import java.util.List;

/**
 *
 * @author wiston.altamirano
 */
public interface IVehiculoService {

    List<Vehiculos> lstVehiculo();

    Vehiculos buscarID(Long idVehiculo);

    Vehiculos agregar(Vehiculos vehiculo);

    Vehiculos editar(Vehiculos vehiculo);

    Vehiculos remover(Vehiculos vehiculo);
}
