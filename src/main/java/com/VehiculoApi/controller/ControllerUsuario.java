/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.controller;

import com.VehiculoApi.model.seguridad.Usuarios;
import com.VehiculoApi.services.IUsuarioServices;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wiston.altamirano
 */
@RestController
@RequestMapping({"usuario"})
public class ControllerUsuario {

    @Autowired
    IUsuarioServices usuarioService;

    @GetMapping("/list")
    /**
     * *
     * retorna un objeto de tipo List con los datos de los Usuarios
     */
    public List<Usuarios> listar() {
        return usuarioService.lstUsuarios();
    }

    @PostMapping("/save")
    public Usuarios guardar(@Valid @RequestBody Usuarios usuarios) throws Exception {
        usuarios.setPassword(encryptPassword(usuarios.getPassword()));
        return usuarioService.agregar(usuarios);
    }

    @GetMapping("/logIn")
    @ResponseBody
    /**
     * *
     * permitira el login al usuario validando que las credenciales ingresadas
     * por el usuario sean correctas
     */
    public String logIn(@RequestParam String username, @RequestParam String password) throws Exception {
        Usuarios us = usuarioService.buscarUserName(username);

        if (us.getPassword().equals(encryptPassword(password))) {
            return "Acceso Condedido";
        } else {
            return "Credenciales Incorrectas";
        }
    }

    /**
     * *
     * Encriptara el pasword dijitado por el usuario a SHA1
     * @param password
     * @return
     */
    private static String encryptPassword(String password) {
        String sha1 = "";
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return sha1;
    }

    /**
     * *
     *
     * @param hash
     * @return
     */
    private static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
