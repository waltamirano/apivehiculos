/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.controller;

import com.VehiculoApi.model.Cliente;
import com.VehiculoApi.model.Persona;
import com.VehiculoApi.services.IClienteService;
import com.VehiculoApi.services.IPersonaService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wiston.altamirano
 */
@RestController
@RequestMapping({"cliente"})
public class Controlador {
 
    @Autowired
    IClienteService clientesService;
    
    @Autowired
    IPersonaService personaService;
    
    @GetMapping("/listar")
    /****
     * Devuelve ub objeto de tipos lista Cliente 
     * que contiene los registro al macenados en la base de datos
     */
    public List<Cliente> listar(){
        return clientesService.lstClientes();
    }
    
    @PostMapping("/save")
    /***
     * Meotdo para el registro de nuevos Clientes
     * retorna los datos del cliente una vez se ha guardado
     */
    public Cliente guardarCliente(@Valid @RequestBody Cliente cliente)throws Exception{
         //obteniendo los datos de persona para guardar
         Persona pr = personaService.agregar(cliente.getPersona());
         //agregando el identificador de la nueva persona
         cliente.getPersona().setId(pr.getId());

        return clientesService.agregar(cliente);
    }
}
