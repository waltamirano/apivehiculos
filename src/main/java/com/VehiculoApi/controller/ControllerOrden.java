/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.controller;

import com.VehiculoApi.model.Cliente;
import com.VehiculoApi.model.Orden;
import com.VehiculoApi.services.IClienteService;
import com.VehiculoApi.services.IOrdenesService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wiston.altamirano
 */
@RestController
@RequestMapping({"orden"})
public class ControllerOrden {
    
    @Autowired
    IOrdenesService ordenService;
    
    @Autowired
    IClienteService clienteService;
    
    @PostMapping("/save")
    public Orden guardar(@Valid @RequestBody Orden orden)throws  Exception{
        
        return ordenService.guardar(orden);
    }
}
