/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.controller;

import com.VehiculoApi.model.Vehiculos;
import com.VehiculoApi.services.IVehiculoService;
import java.util.Date;
import java.util.List;
import javax.print.attribute.standard.DialogTypeSelection;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wiston.altamirano
 */
@RestController
@RequestMapping({"auto"})
public class ControllerVehiculo {

    @Autowired
    IVehiculoService vehiculoService;

    @GetMapping("/list")
    /**
     * *
     * retorna un objeto de tipo List con los datos de los vehiculos
     */
    public List<Vehiculos> listar() {
        return vehiculoService.lstVehiculo();
    }

    @GetMapping("/precio")
    @ResponseBody
    /**
     * *
     * Devuelve el precio del alquiler segun la cantidad de dias
     */
    public double precioAlquiler(@RequestParam(value = "fechaIni") @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaIni,
            @RequestParam(value = "fechaFin") @DateTimeFormat(pattern = "dd/MM/yyyy") Date fechaFin, @RequestParam(value = "idvehiculo") Long idvehiculo) {
        Date fechaI = null, fechaF = null;
//     
        fechaI = fechaIni;
        fechaF = fechaFin;
//           

        Vehiculos vh = vehiculoService.buscarID(idvehiculo);
        int milisecondsByDay = 86400000;
        int dias = (int) ((fechaF.getTime() - fechaI.getTime()) / milisecondsByDay);

        double costo = vh.getCostoalquiler();
        if (dias >= 1) {
            costo = costo * dias;
        }

        return costo;

    }

    @PostMapping("/save")
    public Vehiculos guardar(@Valid @RequestBody Vehiculos vehiculo) throws Exception {
        return vehiculoService.agregar(vehiculo);
    }
}
