/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.VehiculoApi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author wiston.altamirano
 */
@Entity
@Table(name = "vehiculos", schema = "alquiler")
public class Vehiculos implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String Modelo;

    @Column
    private String Marca;

    @Column
    private boolean disponible;

    @Column
    private int numeroPasajeros;

    @Column
    private String tipo; //Almacenara tipo de vehiculo eje: Suv, Sedan, Camioneta

    @Column
    private boolean dobleTraccion;

    @Column
    private double costoalquiler;

    @OneToMany(mappedBy = "vehiculo", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Orden> lstOrden;

    public Vehiculos() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String Modelo) {
        this.Modelo = Modelo;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String Marca) {
        this.Marca = Marca;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public int getNumeroPasajeros() {
        return numeroPasajeros;
    }

    public void setNumeroPasajeros(int numeroPasajeros) {
        this.numeroPasajeros = numeroPasajeros;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isDobleTraccion() {
        return dobleTraccion;
    }

    public void setDobleTraccion(boolean dobleTraccion) {
        this.dobleTraccion = dobleTraccion;
    }

    public double getCostoalquiler() {
        return costoalquiler;
    }

    public void setCostoalquiler(double costoalquiler) {
        this.costoalquiler = costoalquiler;
    }

    public List<Orden> getLstOrden() {
        return lstOrden;
    }

    public void setLstOrden(List<Orden> lstOrden) {
        this.lstOrden = lstOrden;
    }

}
